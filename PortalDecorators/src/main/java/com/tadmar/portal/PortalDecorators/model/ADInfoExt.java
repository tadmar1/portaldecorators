package com.tadmar.portal.PortalDecorators.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ADInfoExt implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String userSgid;
	private String clientIP;
	private ADInfo adinfo;
	
	public String getUserSgid() {
		return userSgid;
	}
	public void setUserSgid(String userSgid) {
		this.userSgid = userSgid;
	}
	public String getClientIP() {
		return clientIP;
	}
	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}
	public ADInfo getAdinfo() {
		return adinfo;
	}
	public void setAdinfo(ADInfo adinfo) {
		this.adinfo = adinfo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adinfo == null) ? 0 : adinfo.hashCode());
		result = prime * result
				+ ((clientIP == null) ? 0 : clientIP.hashCode());
		result = prime * result
				+ ((userSgid == null) ? 0 : userSgid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ADInfoExt))
			return false;
		ADInfoExt other = (ADInfoExt) obj;
		if (adinfo == null) {
			if (other.adinfo != null)
				return false;
		} else if (!adinfo.equals(other.adinfo))
			return false;
		if (clientIP == null) {
			if (other.clientIP != null)
				return false;
		} else if (!clientIP.equals(other.clientIP))
			return false;
		if (userSgid == null) {
			if (other.userSgid != null)
				return false;
		} else if (!userSgid.equals(other.userSgid))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ADInfoExt [userSgid=" + userSgid + ", clientIP=" + clientIP
				+ ", adinfo=" + adinfo + "]";
	}
}
