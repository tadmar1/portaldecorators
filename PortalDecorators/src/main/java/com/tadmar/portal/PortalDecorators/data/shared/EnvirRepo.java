package com.tadmar.portal.PortalDecorators.data.shared;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.tadmar.portal.PortalDecorators.model.shared.CusrDa;
import com.tadmar.portal.PortalDecorators.util.SqlExpressionService;


@Stateless
public class EnvirRepo {

	@Inject SqlExpressionService sqlExpressionService;
	@SuppressWarnings("unused")
	@Inject private transient Logger logger;

	@PersistenceContext(unitName = "SG_DANEORGANIZACJA")
    private EntityManager emSp;

	@PersistenceContext(unitName = "S65BCC4B")
    private EntityManager emMvx;

	public CusrDa findById( String cusrid) {
    	@SuppressWarnings("unchecked")
		List<CusrDa> cusrdaList =   emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("EnvirRepo.CusrDaFindById")
    			.replace("{0}", cusrid)
				,CusrDa.class).getResultList();
    	return cusrdaList.isEmpty() ? new CusrDa() : cusrdaList.get(0);
	}

	public CusrDa findByRole( String curoid) {
    	@SuppressWarnings("unchecked")
		List<CusrDa> cusrdaList =   emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("EnvirRepo.CusrDaFindByRole")
    			.replace("{0}", curoid)
				,CusrDa.class).getResultList();
    	return cusrdaList.isEmpty() ? new CusrDa() : cusrdaList.get(0);
	}

	public CusrDa findByAdditionalRole( String curoid) {
    	@SuppressWarnings("unchecked")
		List<CusrDa> cusrdaList =   emSp.createNativeQuery(sqlExpressionService.getSqlExpressionEntry("EnvirRepo.CusrDaFindByAdditionalRole")
    			.replace("{0}", curoid)
				,CusrDa.class).getResultList();
    	return cusrdaList.isEmpty() ? new CusrDa() : cusrdaList.get(0);
	}

}
