package com.tadmar.portal.PortalDecorators.controller;

import java.io.Serializable;
import java.util.Base64;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.tadmar.portal.PortalDecorators.model.ADInfo;
import com.tadmar.portal.PortalDecorators.model.shared.CusrDa;


@Named(value="ADUsersAndGroupsCtrl")
@ViewScoped
public class ADUsersAndGroupsCtrl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private CusrDa cusrda;
	private String xxx;
	
	/********************************************************************************************************
	 *  Constructors
	 ********************************************************************************************************/
	@PostConstruct
	private void init() {
		cusrda = new CusrDa();
		xxx="dddd";
	}

	/********************************************************************************************************
	 *  Events
	 ********************************************************************************************************/
	public void onClick() {
		
	}
	/********************************************************************************************************
	 *  Methods
	 ********************************************************************************************************/
	/********************************************************************************************************
	 *  Get-Set
	 ********************************************************************************************************/
	public CusrDa getCusrda() {
		return cusrda;
	}
	public void setCusrda(CusrDa cusrda) {
		this.cusrda = cusrda;
	}

	public String getXxx() {
		return xxx;
	}

	public void setXxx(String xxx) {
		this.xxx = xxx;
	}
}
