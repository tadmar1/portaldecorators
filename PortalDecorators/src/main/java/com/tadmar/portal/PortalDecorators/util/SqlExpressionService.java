package com.tadmar.portal.PortalDecorators.util;

import java.io.IOException;
import java.io.StringReader;

import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
@Singleton
public class SqlExpressionService {
	
	private static final String LEFT_INDENT_TO_OMMIT = "\t\t\t\t\t\t\t\t\t\t";
	
	@Inject SqlExpressionRepository sqlExpressionRepository;
	private Document doc;
	
	@PostConstruct
	public void init() {
		this.doc = readSqlQueryFile();
	}	

	public Document readSqlQueryFile() {
	    try {
		    InputSource is = new InputSource();
		    is.setCharacterStream(new StringReader(sqlExpressionRepository.getEntry()));
		    return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is);
		} catch (SAXException | IOException | ParserConfigurationException | FactoryConfigurationError e) {
			e.printStackTrace();
		}
	    return null;
	}
	
	public String getSqlExpressionEntry(String searchKey) {
	
		try {
			XPathExpression expr = XPathFactory.newInstance().newXPath().compile("/root/data[@name=\"" + 
					searchKey + "\"]/value");
			return ((String) expr.evaluate(doc, XPathConstants.STRING)).replace("\n" + LEFT_INDENT_TO_OMMIT , "\n");
		} catch (Exception parserException ){
			return "";
		}
	}

	public Document getDoc() {
		return doc;
	}

	public void setDoc(Document doc) {
		this.doc = doc;
	}
}
