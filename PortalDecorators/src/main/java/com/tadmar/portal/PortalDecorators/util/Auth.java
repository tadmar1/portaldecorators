package com.tadmar.portal.PortalDecorators.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

public class Auth {
	
	public static final String getUserName() {
		HttpServletRequest servletRequest = (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
	    String userName = servletRequest.getUserPrincipal().getName().toUpperCase().trim().replace("ZD\\", "").replace("ZD/", "");
	    return userName;
	}
}
