package com.tadmar.portal.PortalDecorators.service;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

import com.tadmar.portal.PortalDecorators.model.ADInfo;
@Startup
@Named
@ApplicationScoped
public class ADPersonGlobalServ implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject private ADSearch adsearchServ;
	private List<ADInfo> adinfoList;
	
	@PostConstruct
	private void init() {
		LdapContext ctx = adsearchServ.getLdapContext();
		adinfoList = adsearchServ.getAllUserList(ctx);
		try {
			ctx.close();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		System.out.println("Global start");
	}

	public List<ADInfo> getAdinfoList() {
		return adinfoList;
	}
	public void setAdinfoList(List<ADInfo> adinfoList) {
		this.adinfoList = adinfoList;
	}
}
