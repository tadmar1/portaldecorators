package com.tadmar.portal.PortalDecorators.service;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import com.tadmar.portal.PortalDecorators.model.ADInfo;

@Named(value="ADSearch")
@ViewScoped
public class ADSearch implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String LDAP_SECURITY_AUTHENTICATION= "Simple";
	private static final String LDAP_SECURITY_PRINCIPAL = "ZD\\R6492682";
	private static final String LDAP_SECURITY_CREDENTIALS = "Romjas9876$";
	private static final String LDAP_PROVIDER_URL = "ldap://d00dcplpoz01.zd.if.atcsg.net:389";
	public static final String IMG_BASE64_PREFIX="data:image/jpeg;base64,";
	@PostConstruct
	private void init() {}
	
	/********************************************************************************************************
	 *  Methods
	 ********************************************************************************************************/
	public List<ADInfo> getAllUserList(LdapContext ctx) {

		List<ADInfo> adinfoList = new ArrayList<ADInfo>();
		String[] attrIDs = {"sAMAccountName", "cn"};
		String searchName ="OU=Users,OU=Tadmar,OU=Locations,OU=D18-Tadmar,DC=zd,DC=if, DC=atcsg, DC=net";
		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		constraints.setReturningAttributes(attrIDs);
		try {
			NamingEnumeration<SearchResult> answer = ctx.search(searchName,"(objectclass=person)" , constraints);
			while (answer.hasMore()) {
				Attributes attrs = ((SearchResult) answer.next()).getAttributes();
				ADInfo adinfo = new ADInfo();
				adinfo.setSamAccountName(attrs.get("sAMAccountName").get(0).toString());
				adinfo.setCn(attrs.get("cn").get(0).toString());
				adinfoList.add(adinfo);
			}
		} catch (NamingException e) {
		}
		
		return adinfoList;
	}

	public ADInfo getUserBasicAttributes(String username, LdapContext ctx, String[] attrIDs, String excludeAttr) {
		
		ADInfo adinfo = new ADInfo();
		//ADInfoG adinfoG = new ADInfoG();
		
		Class<ADInfo> ADInfoClass = ADInfo.class;
		Field[] fields = ADInfoClass.getDeclaredFields();
		
		try {
			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			constraints.setReturningAttributes(attrIDs);
			NamingEnumeration<SearchResult> answer = ctx.search("DC=zd,DC=if, DC=atcsg, DC=net", "sAMAccountName="
                + username, constraints);
			if (answer.hasMore()) {
				Attributes attrs = ((SearchResult) answer.next()).getAttributes();
				for(int i =0; i < fields.length; i++ ){
					if (!fields[i].getName().startsWith("notRetrieved") && !fields[i].getName().equals(excludeAttr)) {
						if ( !fields[i].getName().equals("thumbnailPhoto") ) {
							try {
								if ( fields[i].getName().startsWith("manager")) {
									//fields[i].set(adinfo,attrs.get(fields[i].getName())
									//		.get(0).toString().replace("\\,", " ").split(",")[0]
									//		.replace("CN=", ""));
								} else if ( fields[i].getName().startsWith("directReports")) {
									String tmpField = "";
									int numLines = attrs.get(fields[i].getName()).size();
									for ( int z = 0; z < numLines; z++) {
										if ( z > 0 ) {
											tmpField += ", ";
										}
										tmpField += attrs.get(fields[i].getName()).get(z).toString()
										.replace("\\,", " ").split(",")[0].replace("CN=", "");
									}
									//fields[i].set(adinfo, tmpField);
								} else {
									//fields[i].set(adinfo,attrs.get(fields[i].getName()).get(0).toString());
								}
							} catch(NullPointerException npe) {
								//fields[i].set(adinfo,"");
							}
						} else {
							try {
								Object photo = attrs.get(fields[i].getName()).get(0);
								byte[] buf = (byte[])photo;
								//fields[i].set(adinfo,buf);
								String base64encodedString = Base64.getEncoder().encodeToString(buf);
								//		.replace("_", "/").replace("-", "+");
								adinfo.setNotRetrieved(IMG_BASE64_PREFIX + base64encodedString);
					            //FileOutputStream imageOutFile = new FileOutputStream(
					            //        "c:\\temp\\convert.jpg");
					            //imageOutFile.write(buf);
					            //imageOutFile.close();
							} catch (NullPointerException npe) {}
						}
					}
				}
			}else{
				throw new IllegalArgumentException("Invalid User");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		//adinfo = (ADInfo) adinfoG.;
		return  adinfo;
	}

	public LdapContext getLdapContext(){
		LdapContext ctx = null;
		try{
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.SECURITY_AUTHENTICATION, LDAP_SECURITY_AUTHENTICATION);
			env.put(Context.SECURITY_PRINCIPAL, LDAP_SECURITY_PRINCIPAL);
			env.put(Context.SECURITY_CREDENTIALS, LDAP_SECURITY_CREDENTIALS);
			env.put(Context.PROVIDER_URL, LDAP_PROVIDER_URL);
			ctx = new InitialLdapContext(env, null);
		}catch(NamingException nex){
			System.out.println("LDAP Connection: FAILED");
			nex.printStackTrace();
		}
		return ctx;
	}

	@SuppressWarnings("rawtypes")
	public String[] getUserAttrIDs() {
		
		Class ADInfoClass = ADInfo.class;
		Field[] fields = ADInfoClass.getDeclaredFields();
		String[] fieldsNames = new String[fields.length];
		for ( int i = 0; i < fields.length; i++) {
			fieldsNames[i] = fields[i].getName();
		}	
		return fieldsNames;
	}
	
	//public static String encode64Image(byte[] imageByteArray) {
    //    return Base64.encodeBase64URLSafeString(imageByteArray);
    //}
	
	//public static byte[] decode64Image(String imageDataString) {
    //    return Base64.decodeBase64(imageDataString);
    //}

	

}
