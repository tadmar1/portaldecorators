package com.tadmar.portal.PortalDecorators.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.tadmar.portal.PortalDecorators.data.shared.EnvirRepo;
import com.tadmar.portal.PortalDecorators.model.ADInfoExt;
import com.tadmar.portal.PortalDecorators.model.shared.CusrDa;
import com.tadmar.portal.PortalDecorators.util.Utils;

@Named(value="HeaderCtrl")
@ViewScoped
public class HeaderCtrl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CusrDa cusrda;
	@Inject private EnvirRepo envirRepo;
	
	private ADInfoExt aDinfoExt;
	/********************************************************************************************************
	 *  Constructors
	 ********************************************************************************************************/
	@PostConstruct
	private void init() {
		cusrda = envirRepo.findById(Utils.getUserName());
		onRetrieveUserInfo();
	}

	/********************************************************************************************************
	 *  Events
	 ********************************************************************************************************/
	public void onRetrieveUserInfo() {
		aDinfoExt = RetrieveLdapUserInfo(cusrda.getCusrid());
	}
	/********************************************************************************************************
	 *  Methods
	 ********************************************************************************************************/
	private ADInfoExt RetrieveLdapUserInfo(String userSgid) {
		
		ADInfoExt adinfoExt = new ADInfoExt();
		adinfoExt.setUserSgid(userSgid);
		adinfoExt.setClientIP(org.omnifaces.util.Faces.getRemoteAddr());
		return adinfoExt;
	}
	
	/********************************************************************************************************
	 *  Get-Set
	 ********************************************************************************************************/
	public CusrDa getCusrda() {
		return cusrda;
	}
	public void setCusrda(CusrDa cusrda) {
		this.cusrda = cusrda;
	}
	public EnvirRepo getEnvirRepo() {
		return envirRepo;
	}
	public void setEnvirRepo(EnvirRepo envirRepo) {
		this.envirRepo = envirRepo;
	}
	public ADInfoExt getaDinfoExt() {
		return aDinfoExt;
	}
	public void setaDinfoExt(ADInfoExt aDinfoExt) {
		this.aDinfoExt = aDinfoExt;
	}
}
