package com.tadmar.portal.PortalDecorators.model;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Id;

@SuppressWarnings("serial")
public class ADInfoG implements Serializable{

	/**
	 * 
	 */
	@Id
	public String sAMAccountName;
	public String distinguishedName;
	public String cn;
	public String uid;
	// General
	public String givenName;
	public String initials;
	public String sn;
	public String displayName;
	public String description;
	public String physicalDeliveryOfficeName;
	public String telephoneNumber;
	public String mail;
	public String wWWHomepage;
	//Address
	public String streetAddress;
	public String postOfficeBox;
	public String L;  //city
	public String st;
	public String postalCode;
	public String co; //country
	//Phone
	//public String telephoneNumber;
	public String otherTelephone;
	public String facsimileTelephoneNumber;
	public String telephoneAssistant;
	public String homePhone;
	public String otherHomePhone;
	public String mobile;
	public String pager;
	public String info;
	public byte[] thumbnailPhoto;
	public String notRetrieved;
	//Organization 
	public String title;
	public String department;
	public String company;
	public String manager;
	public String directReports;
	
	public String getsAMAccountName() {
		return sAMAccountName;
	}
	public void setsAMAccountName(String sAMAccountName) {
		this.sAMAccountName = sAMAccountName;
	}
	public String getDistinguishedName() {
		return distinguishedName;
	}
	public void setDistinguishedName(String distinguishedName) {
		this.distinguishedName = distinguishedName;
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getInitials() {
		return initials;
	}
	public void setInitials(String initials) {
		this.initials = initials;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPhysicalDeliveryOfficeName() {
		return physicalDeliveryOfficeName;
	}
	public void setPhysicalDeliveryOfficeName(String physicalDeliveryOfficeName) {
		this.physicalDeliveryOfficeName = physicalDeliveryOfficeName;
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getwWWHomepage() {
		return wWWHomepage;
	}
	public void setwWWHomepage(String wWWHomepage) {
		this.wWWHomepage = wWWHomepage;
	}
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public String getPostOfficeBox() {
		return postOfficeBox;
	}
	public void setPostOfficeBox(String postOfficeBox) {
		this.postOfficeBox = postOfficeBox;
	}
	public String getL() {
		return L;
	}
	public void setL(String l) {
		L = l;
	}
	public String getSt() {
		return st;
	}
	public void setSt(String st) {
		this.st = st;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getCo() {
		return co;
	}
	public void setCo(String co) {
		this.co = co;
	}
	public String getOtherTelephone() {
		return otherTelephone;
	}
	public void setOtherTelephone(String otherTelephone) {
		this.otherTelephone = otherTelephone;
	}
	public String getFacsimileTelephoneNumber() {
		return facsimileTelephoneNumber;
	}
	public void setFacsimileTelephoneNumber(String facsimileTelephoneNumber) {
		this.facsimileTelephoneNumber = facsimileTelephoneNumber;
	}
	public String getTelephoneAssistant() {
		return telephoneAssistant;
	}
	public void setTelephoneAssistant(String telephoneAssistant) {
		this.telephoneAssistant = telephoneAssistant;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public String getOtherHomePhone() {
		return otherHomePhone;
	}
	public void setOtherHomePhone(String otherHomePhone) {
		this.otherHomePhone = otherHomePhone;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPager() {
		return pager;
	}
	public void setPager(String pager) {
		this.pager = pager;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public byte[] getThumbnailPhoto() {
		return thumbnailPhoto;
	}
	public void setThumbnailPhoto(byte[] thumbnailPhoto) {
		this.thumbnailPhoto = thumbnailPhoto;
	}
	public String getNotRetrieved() {
		return notRetrieved;
	}
	public void setNotRetrieved(String notRetrieved) {
		this.notRetrieved = notRetrieved;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	public String getDirectReports() {
		return directReports;
	}
	public void setDirectReports(String directReports) {
		this.directReports = directReports;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((L == null) ? 0 : L.hashCode());
		result = prime * result + ((cn == null) ? 0 : cn.hashCode());
		result = prime * result + ((co == null) ? 0 : co.hashCode());
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime * result
				+ ((department == null) ? 0 : department.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((directReports == null) ? 0 : directReports.hashCode());
		result = prime * result
				+ ((displayName == null) ? 0 : displayName.hashCode());
		result = prime
				* result
				+ ((distinguishedName == null) ? 0 : distinguishedName
						.hashCode());
		result = prime
				* result
				+ ((facsimileTelephoneNumber == null) ? 0
						: facsimileTelephoneNumber.hashCode());
		result = prime * result
				+ ((givenName == null) ? 0 : givenName.hashCode());
		result = prime * result
				+ ((homePhone == null) ? 0 : homePhone.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result
				+ ((initials == null) ? 0 : initials.hashCode());
		result = prime * result + ((mail == null) ? 0 : mail.hashCode());
		result = prime * result + ((manager == null) ? 0 : manager.hashCode());
		result = prime * result + ((mobile == null) ? 0 : mobile.hashCode());
		result = prime * result
				+ ((notRetrieved == null) ? 0 : notRetrieved.hashCode());
		result = prime * result
				+ ((otherHomePhone == null) ? 0 : otherHomePhone.hashCode());
		result = prime * result
				+ ((otherTelephone == null) ? 0 : otherTelephone.hashCode());
		result = prime * result + ((pager == null) ? 0 : pager.hashCode());
		result = prime
				* result
				+ ((physicalDeliveryOfficeName == null) ? 0
						: physicalDeliveryOfficeName.hashCode());
		result = prime * result
				+ ((postOfficeBox == null) ? 0 : postOfficeBox.hashCode());
		result = prime * result
				+ ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result
				+ ((sAMAccountName == null) ? 0 : sAMAccountName.hashCode());
		result = prime * result + ((sn == null) ? 0 : sn.hashCode());
		result = prime * result + ((st == null) ? 0 : st.hashCode());
		result = prime * result
				+ ((streetAddress == null) ? 0 : streetAddress.hashCode());
		result = prime
				* result
				+ ((telephoneAssistant == null) ? 0 : telephoneAssistant
						.hashCode());
		result = prime * result
				+ ((telephoneNumber == null) ? 0 : telephoneNumber.hashCode());
		result = prime * result + Arrays.hashCode(thumbnailPhoto);
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((uid == null) ? 0 : uid.hashCode());
		result = prime * result
				+ ((wWWHomepage == null) ? 0 : wWWHomepage.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ADInfoG))
			return false;
		ADInfoG other = (ADInfoG) obj;
		if (L == null) {
			if (other.L != null)
				return false;
		} else if (!L.equals(other.L))
			return false;
		if (cn == null) {
			if (other.cn != null)
				return false;
		} else if (!cn.equals(other.cn))
			return false;
		if (co == null) {
			if (other.co != null)
				return false;
		} else if (!co.equals(other.co))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (directReports == null) {
			if (other.directReports != null)
				return false;
		} else if (!directReports.equals(other.directReports))
			return false;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (distinguishedName == null) {
			if (other.distinguishedName != null)
				return false;
		} else if (!distinguishedName.equals(other.distinguishedName))
			return false;
		if (facsimileTelephoneNumber == null) {
			if (other.facsimileTelephoneNumber != null)
				return false;
		} else if (!facsimileTelephoneNumber
				.equals(other.facsimileTelephoneNumber))
			return false;
		if (givenName == null) {
			if (other.givenName != null)
				return false;
		} else if (!givenName.equals(other.givenName))
			return false;
		if (homePhone == null) {
			if (other.homePhone != null)
				return false;
		} else if (!homePhone.equals(other.homePhone))
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (initials == null) {
			if (other.initials != null)
				return false;
		} else if (!initials.equals(other.initials))
			return false;
		if (mail == null) {
			if (other.mail != null)
				return false;
		} else if (!mail.equals(other.mail))
			return false;
		if (manager == null) {
			if (other.manager != null)
				return false;
		} else if (!manager.equals(other.manager))
			return false;
		if (mobile == null) {
			if (other.mobile != null)
				return false;
		} else if (!mobile.equals(other.mobile))
			return false;
		if (notRetrieved == null) {
			if (other.notRetrieved != null)
				return false;
		} else if (!notRetrieved.equals(other.notRetrieved))
			return false;
		if (otherHomePhone == null) {
			if (other.otherHomePhone != null)
				return false;
		} else if (!otherHomePhone.equals(other.otherHomePhone))
			return false;
		if (otherTelephone == null) {
			if (other.otherTelephone != null)
				return false;
		} else if (!otherTelephone.equals(other.otherTelephone))
			return false;
		if (pager == null) {
			if (other.pager != null)
				return false;
		} else if (!pager.equals(other.pager))
			return false;
		if (physicalDeliveryOfficeName == null) {
			if (other.physicalDeliveryOfficeName != null)
				return false;
		} else if (!physicalDeliveryOfficeName
				.equals(other.physicalDeliveryOfficeName))
			return false;
		if (postOfficeBox == null) {
			if (other.postOfficeBox != null)
				return false;
		} else if (!postOfficeBox.equals(other.postOfficeBox))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		if (sAMAccountName == null) {
			if (other.sAMAccountName != null)
				return false;
		} else if (!sAMAccountName.equals(other.sAMAccountName))
			return false;
		if (sn == null) {
			if (other.sn != null)
				return false;
		} else if (!sn.equals(other.sn))
			return false;
		if (st == null) {
			if (other.st != null)
				return false;
		} else if (!st.equals(other.st))
			return false;
		if (streetAddress == null) {
			if (other.streetAddress != null)
				return false;
		} else if (!streetAddress.equals(other.streetAddress))
			return false;
		if (telephoneAssistant == null) {
			if (other.telephoneAssistant != null)
				return false;
		} else if (!telephoneAssistant.equals(other.telephoneAssistant))
			return false;
		if (telephoneNumber == null) {
			if (other.telephoneNumber != null)
				return false;
		} else if (!telephoneNumber.equals(other.telephoneNumber))
			return false;
		if (!Arrays.equals(thumbnailPhoto, other.thumbnailPhoto))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (uid == null) {
			if (other.uid != null)
				return false;
		} else if (!uid.equals(other.uid))
			return false;
		if (wWWHomepage == null) {
			if (other.wWWHomepage != null)
				return false;
		} else if (!wWWHomepage.equals(other.wWWHomepage))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ADInfo [sAMAccountName=" + sAMAccountName
				+ ", distinguishedName=" + distinguishedName + ", cn=" + cn
				+ ", uid=" + uid + ", givenName=" + givenName + ", initials="
				+ initials + ", sn=" + sn + ", displayName=" + displayName
				+ ", description=" + description
				+ ", physicalDeliveryOfficeName=" + physicalDeliveryOfficeName
				+ ", telephoneNumber=" + telephoneNumber + ", mail=" + mail
				+ ", wWWHomepage=" + wWWHomepage + ", streetAddress="
				+ streetAddress + ", postOfficeBox=" + postOfficeBox + ", L="
				+ L + ", st=" + st + ", postalCode=" + postalCode + ", co="
				+ co + ", otherTelephone=" + otherTelephone
				+ ", facsimileTelephoneNumber=" + facsimileTelephoneNumber
				+ ", telephoneAssistant=" + telephoneAssistant + ", homePhone="
				+ homePhone + ", otherHomePhone=" + otherHomePhone
				+ ", mobile=" + mobile + ", pager=" + pager + ", info=" + info
				+ ", thumbnailPhoto=" + Arrays.toString(thumbnailPhoto)
				+ ", notRetrieved=" + notRetrieved + ", title=" + title
				+ ", department=" + department + ", company=" + company
				+ ", manager=" + manager + ", directReports=" + directReports
				+ "]";
	}
}
