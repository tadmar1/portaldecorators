package com.tadmar.portal.PortalDecorators.util;

import java.io.IOException;

import javax.ejb.Stateless;

import org.apache.commons.io.IOUtils;

@Stateless
public class SqlExpressionRepository {

	private static String SQL_EXPRESSIONS_FILE = "SqlExpressions.xml";
	private String entry;
	
	public SqlExpressionRepository() {
		setEntry(getFile(SQL_EXPRESSIONS_FILE));
	}
	
	private String getFile(String fileName) {
		 
		String result = "";
		ClassLoader classLoader = getClass().getClassLoader();
		try {
		    result = IOUtils.toString(classLoader.getResourceAsStream(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public String getEntry() {
		return entry;
	}
	
	public void setEntry(String entry) {
		this.entry = entry;
	}
}
